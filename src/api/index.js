// 列表请求
import request from "../util/request";

// 显示列表请求
export function getListAPI(data) {
  return request({
    method: "get",
    url: "/data",
    params: data,
  });
}

// 新增任务请求
export function addTaskAPI(data) {
  return request({
    method: "post",
    url: "/data",
    data,
  });
}

// 删除任务请求
export function delTaskAPI(id) {
  return request({
    method: "delete",
    url: `/data/${id}`,
  });
}

// 获取任务详情
export function fetchTaskAPI(id) {
  return request({
    method: "get",
    url: `/data/${id}`,
  });
}

// 编辑任务
export function editTaskAPI(data) {
  return request({
    method: "put",
    url: `/data/${data.id}`,
    data,
  });
}

// 搜索任务
export function searchTaskAPI(keywords) {
  return request({
    method: "get",
    url: `/data/?name=${keywords}`,
  });
}
